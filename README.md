# Kanboard Ansible

An Ansible playbook for deploying [Kanboard](https://kanboard.org/).

## Features

* Installs all required dependencies (ex. `nginx`, `php`, etc.)
* Sets up `nginx` and a new site called `kanboard`
* Installs the latest version of `kanboard` via Git
* Set up HTTPS using [ansible-role-certbot](https://github.com/geerlingguy/ansible-role-certbot)

## Quick Start

* Ensure that SSH access is available to your hosts.  By default this playbook will use the `./ssh/kanboard` key.  See `ansible.cfg` to change this.
* Clone this repository to your local machine or controlling node.
* Add your server addresses to the `inventory` file per the Ansible format
* Update `vars` as appropriate in the `/vars` directory
  * For `certbot`-specific settings, see documentation at [ansible-role-certbot](https://github.com/geerlingguy/ansible-role-certbot)
* Run `ansible-playbook main.yml`
* 🤞
* You're done!

## Support

This has been tested using the following setup.  It will probably work with other distros that use `apt` to manage packages.

* Digital Ocean Droplet
* Ubuntu 21.10 x64

## TODO

* [ ] Set up email facilities
* [ ] Back-up existing data (if updating an existing installation)
* [ ] Add option to enable plug-in installation from GUI by default
